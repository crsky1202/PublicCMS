package com.publiccms.common.tools;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @ Author     ：haoWen
 * @ Date       ：Created in 20:24 2018/9/15
 * @ Description：时间格式化工具类
 */
public class DateUtils {

    private static final String FULL_DATE_FORMAT_STRING = "yyyy-MM-dd HH:mm:ss";
    public static final String SHORT_DATE_FORMAT_STRING = "yyyy-MM-dd";

    public static  String dateToString(Date date){
        SimpleDateFormat format = new SimpleDateFormat(FULL_DATE_FORMAT_STRING);
        return format.format(date);
    }
}
