-- MemberLevel ---
Field list:

    id:ID
    name:名称
    upgradeScore:升级分数
    upgradeCoefficient:积分系数
    isCheckInfo:能否查询资料
    isDownloadInfo:能否下载资料
    isLineUpCheck:能否排队检测
    isPost:能否发帖
    maxDownloadNum:最大下载次数
    dailyMaxDownload:每天最大下载次数
    queuingInAdvance:提前排队时间
    createTime:创建时间
    updateTime:修改时间
    level:级别

Module list:

    Name:List
        ID:memberLevel_list
        URL：memberLevel/list
        Authorized Url：

    Name:Add/Edit
        ID:memberLevel_add
        URL：memberLevel/add
        Authorized Url：memberLevel/save
    
    Name:Delete
        ID:memberLevel_delete
        URL:
        Authorized Url：memberLevel/delete-- MemberLevel ---
-- Member ---
Field list:

    id:ID
    account:账号
    name:姓名
    company:公司
    tel:手机号
    levelId:会员等级ID
    currentIntegral:当前积分
    accumulativeIntegral:累计积分
    auditStatus:审核状态
    isInsideMember:是否为内部会员
    createTime:创建时间
    updateTime:修改时间

Module list:

    Name:List
        ID:member_list
        URL：member/list
        Authorized Url：

    Name:Add/Edit
        ID:member_add
        URL：member/add
        Authorized Url：member/save
    
    Name:Delete
        ID:member_delete
        URL:
        Authorized Url：member/delete-- Member ---
-- AdditionTerm ---
Field list:

    id:ID
    name:加分项目名称
    score:分值
    createTime:创建时间
    updateTime:修改时间
    orderNum:排序序号

Module list:

    Name:List
        ID:additionTerm_list
        URL：additionTerm/list
        Authorized Url：

    Name:Add/Edit
        ID:additionTerm_add
        URL：additionTerm/add
        Authorized Url：additionTerm/save
    
    Name:Delete
        ID:additionTerm_delete
        URL:
        Authorized Url：additionTerm/delete-- AdditionalApplication ---
Field list:

    id:id
    applicant:申请人
    additionTermId:加分项ID
    score:分值
    createTime:创建时间
    updateTime:修改时间
    auditStatus:审核状态

Module list:

    Name:List
        ID:additionalApplication_list
        URL：additionalApplication/list
        Authorized Url：

    Name:Add/Edit
        ID:additionalApplication_add
        URL：additionalApplication/add
        Authorized Url：additionalApplication/save
    
    Name:Delete
        ID:additionalApplication_delete
        URL:
        Authorized Url：additionalApplication/delete