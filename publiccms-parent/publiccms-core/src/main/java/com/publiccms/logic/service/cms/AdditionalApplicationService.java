package com.publiccms.logic.service.cms;

// Generated 2018-9-16 11:41:23 by com.publiccms.common.generator.SourceGenerator

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.publiccms.entities.cms.AdditionalApplication;
import com.publiccms.logic.dao.cms.AdditionalApplicationDao;
import com.publiccms.common.base.BaseService;
import com.publiccms.common.handler.PageHandler;

/**
 *
 * AdditionalApplicationService
 * 
 */
@Service
@Transactional
public class AdditionalApplicationService extends BaseService<AdditionalApplication> {

    /**

     * @param pageIndex
     * @param pageSize
     * @return results page
     */
    @Transactional(readOnly = true)
    public PageHandler getPage(String name,
                Integer pageIndex, Integer pageSize) {
        return dao.getPage(name,
                pageIndex, pageSize);
    }
    
    @Autowired
    private AdditionalApplicationDao dao;
    
}