package com.publiccms.logic.dao.cms;

// Generated 2018-9-16 11:41:23 by com.publiccms.common.generator.SourceGenerator

import org.springframework.stereotype.Repository;

import com.publiccms.common.base.BaseDao;
import com.publiccms.common.constants.CommonConstants;
import com.publiccms.common.handler.PageHandler;
import com.publiccms.common.handler.QueryHandler;
import com.publiccms.common.tools.CommonUtils;
import com.publiccms.entities.cms.AdditionalApplication;
/**
 *
 * AdditionalApplicationDao
 * 
 */
@Repository
public class AdditionalApplicationDao extends BaseDao<AdditionalApplication> {
    
    /**

     * @param pageIndex
     * @param pageSize
     * @return results page
     */
    public PageHandler getPage(String name, Integer pageIndex, Integer pageSize) {
        QueryHandler queryHandler = getQueryHandler("from AdditionalApplication bean");
        if (CommonUtils.notEmpty(name)) {
            queryHandler.condition("bean.name like :name").setParameter("name", like(name));
        }
        queryHandler.order("bean.id asc");
        return getPage(queryHandler, pageIndex, pageSize);
    }

    @Override
    protected AdditionalApplication init(AdditionalApplication entity) {
        return entity;
    }

}