package com.publiccms.logic.dao.cms;

// Generated 2018-9-14 10:36:33 by com.publiccms.common.generator.SourceGenerator

import com.publiccms.common.base.BaseDao;
import com.publiccms.common.handler.PageHandler;
import com.publiccms.common.handler.QueryHandler;
import com.publiccms.common.tools.CommonUtils;
import com.publiccms.entities.cms.Commentary;
import com.publiccms.entities.cms.Member;
import org.springframework.stereotype.Repository;

/**
 *
 * MemberDao
 * 
 */
@Repository
public class CommentaryDao extends BaseDao<Commentary> {
    
    /**

     * @param name
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return results page
     */
    public PageHandler getPage(String name, String title, String theme,
                String orderType, Integer pageIndex, Integer pageSize) {
        QueryHandler queryHandler = getQueryHandler("from Member bean");
        if (CommonUtils.notEmpty(name)) {
            queryHandler.condition("bean.name like :name").setParameter("name", like(name));
        }
        if (CommonUtils.notEmpty(title)) {
            queryHandler.condition("bean.title = :title").setParameter("title", title);
        }
        if (CommonUtils.notEmpty(theme)) {
            queryHandler.condition("bean.theme = :theme").setParameter("theme", theme);
        }
        if(!ORDERTYPE_ASC.equalsIgnoreCase(orderType)){
            orderType = ORDERTYPE_DESC;
        }
        queryHandler.order("bean.createTime " + orderType);
        return getPage(queryHandler, pageIndex, pageSize);
    }

    @Override
    protected Commentary init(Commentary entity) {
        return entity;
    }
}