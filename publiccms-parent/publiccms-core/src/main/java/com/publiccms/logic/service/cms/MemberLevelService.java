package com.publiccms.logic.service.cms;

// Generated 2018-9-13 23:37:54 by com.publiccms.common.generator.SourceGenerator

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.publiccms.entities.cms.MemberLevel;
import com.publiccms.logic.dao.cms.MemberLevelDao;
import com.publiccms.common.base.BaseService;
import com.publiccms.common.handler.PageHandler;

/**
 *
 * MemberLevelService
 * 
 */
@Service
@Transactional
public class MemberLevelService extends BaseService<MemberLevel> {

    private final MemberLevelDao dao;

    @Autowired
    public MemberLevelService(MemberLevelDao dao) {
        this.dao = dao;
    }

    /**

     * @param name
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return results page
     */
    @Transactional(readOnly = true)
    public PageHandler getPage(String name, 
                String orderType, Integer pageIndex, Integer pageSize) {
        return dao.getPage(name, 
                orderType, pageIndex, pageSize);
    }
}