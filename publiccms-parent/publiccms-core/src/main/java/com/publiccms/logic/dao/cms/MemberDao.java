package com.publiccms.logic.dao.cms;

// Generated 2018-9-14 10:36:33 by com.publiccms.common.generator.SourceGenerator

import org.springframework.stereotype.Repository;

import com.publiccms.common.base.BaseDao;
import com.publiccms.common.handler.PageHandler;
import com.publiccms.common.handler.QueryHandler;
import com.publiccms.common.tools.CommonUtils;
import com.publiccms.entities.cms.Member;
/**
 *
 * MemberDao
 * 
 */
@Repository
public class MemberDao extends BaseDao<Member> {
    
    /**

     * @param name
     * @param auditStatus
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return results page
     */
    public PageHandler      getPage(String name, String auditStatus, String isInsideMember,
                String orderType, Integer pageIndex, Integer pageSize) {
        QueryHandler queryHandler = getQueryHandler("from Member bean");
        if (CommonUtils.notEmpty(name)) {
            queryHandler.condition("bean.name like :name").setParameter("name", like(name));
        }
        if (CommonUtils.notEmpty(auditStatus)) {
            queryHandler.condition("bean.auditStatus = :auditStatus").setParameter("auditStatus", auditStatus);
        }
        if (CommonUtils.notEmpty(isInsideMember)) {
            queryHandler.condition("bean.isInsideMember = :isInsideMember").setParameter("isInsideMember", auditStatus);
        }
        if(!ORDERTYPE_ASC.equalsIgnoreCase(orderType)){
            orderType = ORDERTYPE_DESC;
        }
        queryHandler.order("bean.createTime " + orderType);
        return getPage(queryHandler, pageIndex, pageSize);
    }

    /**

     * @return results page
     */
    public long getMember(String account) {
        QueryHandler queryHandler = getQueryHandler("from Member bean");
        if (CommonUtils.notEmpty(account)) {
            queryHandler.condition("bean.account = :account").setParameter("account", account);
        }
        return count(queryHandler);
    }

    /**

     * @return results page
     */
    public long getLogin(String account,String password) {
        QueryHandler queryHandler = getQueryHandler("from Member bean");
        if (CommonUtils.notEmpty(account)) {
            queryHandler.condition("bean.account = :account").setParameter("account", account);
        }
        if (CommonUtils.notEmpty(password)) {
            queryHandler.condition("bean.password = :password").setParameter("password", password);
        }
        return count(queryHandler);
    }

    @Override
    protected Member init(Member entity) {
        return entity;
    }


}