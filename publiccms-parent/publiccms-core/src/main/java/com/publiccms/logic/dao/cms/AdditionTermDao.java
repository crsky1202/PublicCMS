package com.publiccms.logic.dao.cms;

// Generated 2018-9-16 11:35:03 by com.publiccms.common.generator.SourceGenerator

import org.springframework.stereotype.Repository;

import com.publiccms.common.base.BaseDao;
import com.publiccms.common.constants.CommonConstants;
import com.publiccms.common.handler.PageHandler;
import com.publiccms.common.handler.QueryHandler;
import com.publiccms.common.tools.CommonUtils;
import com.publiccms.entities.cms.AdditionTerm;
/**
 *
 * AdditionTermDao
 * 
 */
@Repository
public class AdditionTermDao extends BaseDao<AdditionTerm> {
    
    /**

     * @param name
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return results page
     */
    public PageHandler getPage(String name, String orderType, Integer pageIndex, Integer pageSize) {
        QueryHandler queryHandler = getQueryHandler("from AdditionTerm bean");
        if (CommonUtils.notEmpty(name)) {
            queryHandler.condition("bean.name like :name").setParameter("name", like(name));
        }
        if(!ORDERTYPE_DESC.equalsIgnoreCase(orderType)){
            orderType = ORDERTYPE_ASC;
        }
        queryHandler.order("bean.orderNum " + orderType);
        return getPage(queryHandler, pageIndex, pageSize);
    }

    @Override
    protected AdditionTerm init(AdditionTerm entity) {
        return entity;
    }

}