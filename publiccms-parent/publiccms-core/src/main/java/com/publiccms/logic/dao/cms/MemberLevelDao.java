package com.publiccms.logic.dao.cms;

// Generated 2018-9-13 23:37:54 by com.publiccms.common.generator.SourceGenerator

import org.springframework.stereotype.Repository;

import com.publiccms.common.base.BaseDao;
import com.publiccms.common.constants.CommonConstants;
import com.publiccms.common.handler.PageHandler;
import com.publiccms.common.handler.QueryHandler;
import com.publiccms.common.tools.CommonUtils;
import com.publiccms.entities.cms.MemberLevel;
/**
 *
 * MemberLevelDao
 * 
 */
@Repository
public class MemberLevelDao extends BaseDao<MemberLevel> {
    
    /**

     * @param name
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return results page
     */
    public PageHandler getPage(String name, 
                String orderType, Integer pageIndex, Integer pageSize) {
        QueryHandler queryHandler = getQueryHandler("from MemberLevel bean");
        if (CommonUtils.notEmpty(name)) {
            queryHandler.condition("bean.name like :name").setParameter("name", like(name));
        }
        if(!ORDERTYPE_ASC.equalsIgnoreCase(orderType)){
            orderType = ORDERTYPE_DESC;
        }
        queryHandler.order("bean.createTime " + orderType);
        return getPage(queryHandler, pageIndex, pageSize);
    }

    @Override
    protected MemberLevel init(MemberLevel entity) {
        return entity;
    }

}