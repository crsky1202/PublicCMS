package com.publiccms.logic.service.cms;

// Generated 2018-9-14 10:36:33 by com.publiccms.common.generator.SourceGenerator

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.publiccms.entities.cms.Member;
import com.publiccms.logic.dao.cms.MemberDao;
import com.publiccms.common.base.BaseService;
import com.publiccms.common.handler.PageHandler;

/**
 *
 * MemberService
 * 
 */
@Service
@Transactional
public class MemberService extends BaseService<Member> {

    private final MemberDao dao;

    @Autowired
    public MemberService(MemberDao dao) {
        this.dao = dao;
    }

    /**

     * @param name
     * @param auditStatus
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return results page
     */
    @Transactional(readOnly = true)
    public PageHandler getPage(String name, String auditStatus, String isInsideMember,
                String orderType, Integer pageIndex, Integer pageSize) {
        return dao.getPage(name, auditStatus, isInsideMember,
                orderType, pageIndex, pageSize);
    }

    /**
     * @return results page
     */
    @Transactional(readOnly = true)
    public long getMember(String account) {
        return dao.getMember(account);
    }

    /**
     * @return results page
     */
    @Transactional(readOnly = true)
    public long getLogin(String account,String password) {
        return dao.getLogin(account,password);
    }
}