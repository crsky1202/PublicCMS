package com.publiccms.logic.service.cms;

// Generated 2018-9-16 11:35:04 by com.publiccms.common.generator.SourceGenerator

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.publiccms.entities.cms.AdditionTerm;
import com.publiccms.logic.dao.cms.AdditionTermDao;
import com.publiccms.common.base.BaseService;
import com.publiccms.common.handler.PageHandler;

/**
 *
 * AdditionTermService
 * 
 */
@Service
@Transactional
public class AdditionTermService extends BaseService<AdditionTerm> {

    /**

     * @param name
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return results page
     */
    @Transactional(readOnly = true)
    public PageHandler getPage(String name, 
                String orderType, Integer pageIndex, Integer pageSize) {
        return dao.getPage(name, 
                orderType, pageIndex, pageSize);
    }
    
    @Autowired
    private AdditionTermDao dao;
    
}