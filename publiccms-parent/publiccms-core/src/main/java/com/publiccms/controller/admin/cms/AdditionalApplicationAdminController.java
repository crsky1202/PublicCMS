package com.publiccms.controller.admin.cms;

// Generated 2018-9-16 11:41:23 by com.publiccms.common.generator.SourceGenerator

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.publiccms.common.tools.*;
import com.publiccms.entities.cms.Member;
import com.publiccms.logic.service.cms.MemberService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.publiccms.common.base.AbstractController;
import com.publiccms.common.constants.CommonConstants;

import com.publiccms.entities.cms.AdditionalApplication;import com.publiccms.entities.sys.SysSite;
import com.publiccms.entities.log.LogOperate;
import com.publiccms.logic.service.log.LogLoginService;
import com.publiccms.logic.service.cms.AdditionalApplicationService;

import java.util.Date;

/**
 *
 * AdditionalApplicationAdminController
 * 
 */
@Controller
@RequestMapping("additionalApplication")
public class AdditionalApplicationAdminController extends AbstractController {

    private final AdditionalApplicationService service;
    private final MemberService memberService;

    private String[] ignoreProperties = new String[]{"id"};

    @Autowired
    public AdditionalApplicationAdminController(AdditionalApplicationService service, MemberService memberService) {
        this.service = service;
        this.memberService = memberService;
    }

    /**
     * @param entity
     * @param _csrf
     * @param request
     * @param session
     * @return operate result
     */
    @RequestMapping("save")
    public String save(AdditionalApplication entity, String _csrf, HttpServletRequest request, HttpSession session,
            ModelMap model) {
    	if (ControllerUtils.verifyNotEquals("_csrf", ControllerUtils.getAdminToken(request), _csrf, model)) {
                return CommonConstants.TEMPLATE_ERROR;
        }
        SysSite site = getSite(request);
        if (null != entity.getId()) {
            entity = service.update(entity.getId(), entity, ignoreProperties);
            logOperateService.save(
                        new LogOperate(site.getId(), ControllerUtils.getAdminFromSession(session).getId(), LogLoginService.CHANNEL_WEB_MANAGER,
                                "update.additionalApplication", RequestUtils.getIpAddress(request), CommonUtils.getDate(), JsonUtils.getString(entity)));
        } else {
            entity.setCreateTime(DateUtils.dateToString(new Date()));
            service.save(entity);
            logOperateService
                    .save(new LogOperate(site.getId(), ControllerUtils.getAdminFromSession(session).getId(), LogLoginService.CHANNEL_WEB_MANAGER,
                            "save.additionalApplication", RequestUtils.getIpAddress(request), CommonUtils.getDate(), JsonUtils.getString(entity)));
        }
        return CommonConstants.TEMPLATE_DONE;
    }

    /**
     * @param ids
     * @param _csrf
     * @param request
     * @param session
     * @return operate result
     */
    @RequestMapping("delete")
    public String delete(Integer[] ids, String _csrf, HttpServletRequest request, HttpSession session,
            ModelMap model) {
    	if (ControllerUtils.verifyNotEquals("_csrf", ControllerUtils.getAdminToken(request), _csrf, model)) {
                return CommonConstants.TEMPLATE_ERROR;
        }
        SysSite site = getSite(request);
        if (CommonUtils.notEmpty(ids)) {
            service.delete(ids);
            logOperateService.save(new LogOperate(site.getId(), ControllerUtils.getAdminFromSession(session).getId(),
                    LogLoginService.CHANNEL_WEB_MANAGER, "delete.additionalApplication", RequestUtils.getIpAddress(request), CommonUtils.getDate(), StringUtils.join(ids, ',')));
        }
        return CommonConstants.TEMPLATE_DONE;
    }

    /**
     * @param id
     * @param _csrf
     * @param request
     * @param session
     * @return operate result
     */
    @RequestMapping("audit")
    public String audit(Integer id,String auditStatus, String _csrf, HttpServletRequest request, HttpSession session,
                         ModelMap model) {
        if (ControllerUtils.verifyNotEquals("_csrf", ControllerUtils.getAdminToken(request), _csrf, model)) {
            return CommonConstants.TEMPLATE_ERROR;
        }
        SysSite site = getSite(request);
        if (CommonUtils.notEmpty(id)) {
            //通过id查询当前要审核的记录
            AdditionalApplication entity = service.getEntity(id);
            //修改审核状态
            entity.setAuditStatus(auditStatus);
            //执行审核操作
            service.update(id,entity,ignoreProperties);
            //加分审核通过之后应该将分数添加到会员的积分中去
            //service.delete(id);
            Member member = memberService.getEntity(entity.getApplicant());
            if (null != member) {
                member.setCurrentIntegral(sum(member.getCurrentIntegral(),entity.getScore()));
                member.setAccumulativeIntegral(sum(member.getAccumulativeIntegral(),entity.getScore()));
                memberService.update(member.getId(),member);
            }
            logOperateService.save(new LogOperate(site.getId(), ControllerUtils.getAdminFromSession(session).getId(),
                    LogLoginService.CHANNEL_WEB_MANAGER, "delete.additionalApplication", RequestUtils.getIpAddress(request), CommonUtils.getDate(), StringUtils.join(id, ',')));
        }
        return CommonConstants.TEMPLATE_DONE;
    }


    private String sum(String first,String second){
        int a = Integer.parseInt(first) + Integer.parseInt(second);
        return String.valueOf(a);
    }
}