package com.publiccms.controller.admin.cms;

// Generated 2018-9-16 11:35:04 by com.publiccms.common.generator.SourceGenerator

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.publiccms.common.tools.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import com.publiccms.common.base.AbstractController;
import com.publiccms.common.constants.CommonConstants;
import com.publiccms.entities.cms.AdditionTerm;import com.publiccms.entities.sys.SysSite;
import com.publiccms.entities.log.LogOperate;
import com.publiccms.logic.service.log.LogLoginService;
import com.publiccms.logic.service.cms.AdditionTermService;
import java.util.Date;

/**
 *
 * AdditionTermAdminController
 * 
 */
@Controller
@RequestMapping("additionTerm")
public class AdditionTermAdminController extends AbstractController {

    private String[] ignoreProperties = new String[]{"id"};
    
    /**
     * @param entity
     * @param _csrf
     * @param request
     * @param session
     * @return operate result
     */
    @RequestMapping("save")
    public String save(AdditionTerm entity, String _csrf, HttpServletRequest request, HttpSession session,
            ModelMap model) {
    	if (ControllerUtils.verifyNotEquals("_csrf", ControllerUtils.getAdminToken(request), _csrf, model)) {
                return CommonConstants.TEMPLATE_ERROR;
        }
        SysSite site = getSite(request);
        if (null != entity.getId()) {
            entity = service.update(entity.getId(), entity, ignoreProperties);
            logOperateService.save(
                        new LogOperate(site.getId(), ControllerUtils.getAdminFromSession(session).getId(), LogLoginService.CHANNEL_WEB_MANAGER,
                                "update.additionTerm", RequestUtils.getIpAddress(request), CommonUtils.getDate(), JsonUtils.getString(entity)));
        } else {
            entity.setCreateTime(DateUtils.dateToString(new Date()));
            service.save(entity);
            logOperateService
                    .save(new LogOperate(site.getId(), ControllerUtils.getAdminFromSession(session).getId(), LogLoginService.CHANNEL_WEB_MANAGER,
                            "save.additionTerm", RequestUtils.getIpAddress(request), CommonUtils.getDate(), JsonUtils.getString(entity)));
        }
        return CommonConstants.TEMPLATE_DONE;
    }

    /**
     * @param ids
     * @param _csrf
     * @param request
     * @param session
     * @return operate result
     */
    @RequestMapping("delete")
    public String delete(Integer[] ids, String _csrf, HttpServletRequest request, HttpSession session,
            ModelMap model) {
    	if (ControllerUtils.verifyNotEquals("_csrf", ControllerUtils.getAdminToken(request), _csrf, model)) {
                return CommonConstants.TEMPLATE_ERROR;
        }
        SysSite site = getSite(request);
        if (CommonUtils.notEmpty(ids)) {
            service.delete(ids);
            logOperateService.save(new LogOperate(site.getId(), ControllerUtils.getAdminFromSession(session).getId(),
                    LogLoginService.CHANNEL_WEB_MANAGER, "delete.additionTerm", RequestUtils.getIpAddress(request), CommonUtils.getDate(), StringUtils.join(ids, ',')));
        }
        return CommonConstants.TEMPLATE_DONE;
    }
    
    @Autowired
    private AdditionTermService service;
}