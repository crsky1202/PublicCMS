package com.publiccms.controller.admin.cms;

// Generated 2018-9-14 10:36:34 by com.publiccms.common.generator.SourceGenerator

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.publiccms.common.tools.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import com.publiccms.common.base.AbstractController;
import com.publiccms.common.constants.CommonConstants;
import com.publiccms.entities.cms.Member;import com.publiccms.entities.sys.SysSite;
import com.publiccms.entities.log.LogOperate;
import com.publiccms.logic.service.log.LogLoginService;
import com.publiccms.logic.service.cms.MemberService;

import java.util.Date;

/**
 *
 * MemberAdminController
 * 
 */
@Controller
@RequestMapping("member")
public class MemberAdminController extends AbstractController {

    private final MemberService service;

    private String[] ignoreProperties = new String[]{"id"};

    @Autowired
    public MemberAdminController(MemberService service) {
        this.service = service;
    }

    /**
     * @param entity
     * @param _csrf
     * @param request
     * @param session
     * @return operate result
     */
    @RequestMapping("save")
    public String save(Member entity, String _csrf, HttpServletRequest request, HttpSession session,
            ModelMap model) {
    	if (ControllerUtils.verifyNotEquals("_csrf", ControllerUtils.getAdminToken(request), _csrf, model)) {
                return CommonConstants.TEMPLATE_ERROR;
        }
        SysSite site = getSite(request);
        if (null != entity.getId()) {
            entity = service.update(entity.getId(), entity, ignoreProperties);
            logOperateService.save(
                        new LogOperate(site.getId(), ControllerUtils.getAdminFromSession(session).getId(), LogLoginService.CHANNEL_WEB_MANAGER,
                                "update.member", RequestUtils.getIpAddress(request), CommonUtils.getDate(), JsonUtils.getString(entity)));
        } else {
            entity.setCreateTime(DateUtils.dateToString(new Date()));
            service.save(entity);
            logOperateService
                    .save(new LogOperate(site.getId(), ControllerUtils.getAdminFromSession(session).getId(), LogLoginService.CHANNEL_WEB_MANAGER,
                            "save.member", RequestUtils.getIpAddress(request), CommonUtils.getDate(), JsonUtils.getString(entity)));
        }
        return CommonConstants.TEMPLATE_DONE;
    }

    /**
     * @param ids
     * @param _csrf
     * @param request
     * @param session
     * @return operate result
     */
    @RequestMapping("delete")
    public String delete(Long[] ids, String _csrf, HttpServletRequest request, HttpSession session,
            ModelMap model) {
    	if (ControllerUtils.verifyNotEquals("_csrf", ControllerUtils.getAdminToken(request), _csrf, model)) {
                return CommonConstants.TEMPLATE_ERROR;
        }
        SysSite site = getSite(request);
        if (CommonUtils.notEmpty(ids)) {
            service.delete(ids);
            logOperateService.save(new LogOperate(site.getId(), ControllerUtils.getAdminFromSession(session).getId(),
                    LogLoginService.CHANNEL_WEB_MANAGER, "delete.member", RequestUtils.getIpAddress(request), CommonUtils.getDate(), StringUtils.join(ids, ',')));
        }
        return CommonConstants.TEMPLATE_DONE;
    }

    /**
     *
     * 审核
     * @param id
     * @param _csrf
     * @param auditStatus
     * @param request
     * @param session
     * @param model
     * @return
     */
    @RequestMapping(value = "audit")
    public String audit(Long id , String _csrf, String auditStatus, HttpServletRequest request , HttpSession session, ModelMap model) {
        if (ControllerUtils.verifyNotEquals("_csrf", ControllerUtils.getAdminToken(request), _csrf, model)) {
            return CommonConstants.TEMPLATE_ERROR;
        }
        SysSite site = getSite(request);
        updateColumnById(id, auditStatus, request, session, site);
        return CommonConstants.TEMPLATE_DONE;
    }


    /**
     *修改是否为内部会员
     * @param id
     * @param _csrf
     * @param memberStatus
     * @param request
     * @param session
     * @param model
     * @return
     */
    @RequestMapping(value = "update_member_status")
    public String updateMemberStatus(Long id , String _csrf, String memberStatus, HttpServletRequest request , HttpSession session, ModelMap model) {
        if (ControllerUtils.verifyNotEquals("_csrf", ControllerUtils.getAdminToken(request), _csrf, model)) {
            return CommonConstants.TEMPLATE_ERROR;
        }
        SysSite site = getSite(request);
        updateColumnById(id, memberStatus, request, session, site);
        return CommonConstants.TEMPLATE_DONE;
    }


    /**
     * 根据id修改字段
     * @param id
     * @param memberStatus
     * @param request
     * @param session
     * @param site
     */
    private void updateColumnById(Long id, String memberStatus, HttpServletRequest request, HttpSession session, SysSite site) {
        if (CommonUtils.notEmpty(id)) {
            Member entity = service.getEntity(id);
            entity.setAuditStatus(memberStatus);
            entity = service.update(id, entity, ignoreProperties);
            logOperateService.save(
                    new LogOperate(site.getId(), ControllerUtils.getAdminFromSession(session).getId(), LogLoginService.CHANNEL_WEB_MANAGER,
                            "update.member", RequestUtils.getIpAddress(request), CommonUtils.getDate(), JsonUtils.getString(entity)));
        }
    }

}