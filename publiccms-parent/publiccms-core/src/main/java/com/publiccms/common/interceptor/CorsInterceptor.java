package com.publiccms.common.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsProcessor;
import org.springframework.web.cors.DefaultCorsProcessor;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.publiccms.logic.component.config.CorsConfigComponent;
import com.publiccms.logic.component.site.SiteComponent;

/**
 * CorsContextInterceptor 权限拦截器
 */
@Component
public class CorsInterceptor extends HandlerInterceptorAdapter {
    private final String FRONT = "front";

    private static final CorsProcessor corsProcessor = new DefaultCorsProcessor();
    @Autowired
    private CorsConfigComponent corsConfigComponent;
    @Autowired
    private SiteComponent siteComponent;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String path = request.getRequestURL().toString();
        //url包含front的已json格式返回，解决跨域问题
        if (path.indexOf(FRONT) > -1) {
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json; charset=utf-8");
        }
        return corsProcessor.processRequest(corsConfigComponent.getConfig(siteComponent.getSite(request.getServerName())),
                request, response);
    }
}
