package com.publiccms.entities.cms;

import com.publiccms.common.database.CmsUpgrader;
import com.publiccms.common.generator.annotation.GeneratorColumn;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;

/**
 * @ Author     ：haoWen
 * @ Date       ：Created in 13:30 2018/9/11
 * @ Description：会员
 */
@Entity
@Table(name = "member",uniqueConstraints = @UniqueConstraint(columnNames = {"name","account"}))
@DynamicUpdate
public class Member implements Serializable {

    private static final long serialVersionUID = 8917849561822541696L;
    @GeneratorColumn(title = "ID")
    private Long id;

    @GeneratorColumn(title = "账号")
    private String account;

    @GeneratorColumn(title = "姓名", condition = true,like = true)
    private String name;

    @GeneratorColumn(title = "公司")
    private String company;

    @GeneratorColumn(title = "手机号")
    private String tel;

    @GeneratorColumn(title = "邮箱")
    private String email;

    @GeneratorColumn(title = "密码")
    private String password;

    @GeneratorColumn(title = "会员等级ID")
    private String levelId;

    @GeneratorColumn(title = "当前积分")
    private String currentIntegral;

    @GeneratorColumn(title = "累计积分")
    private String accumulativeIntegral;

    @GeneratorColumn(title = "审核状态", condition = true)
    private String auditStatus;

    @GeneratorColumn(title = "是否为内部会员")
    private String isInsideMember;

    @GeneratorColumn(title = "创建时间",order = true)
    private String createTime;

    @GeneratorColumn(title = "修改时间")
    private String updateTime;

    public Member() {
    }

    public Member(Long id, String account, String name, String company, String tel, String levelId, String currentIntegral, String accumulativeIntegral, String auditStatus, String isInsideMember, String createTime, String updateTime) {
        this.id = id;
        this.account = account;
        this.name = name;
        this.company = company;
        this.tel = tel;
        this.levelId = levelId;
        this.currentIntegral = currentIntegral;
        this.accumulativeIntegral = accumulativeIntegral;
        this.auditStatus = auditStatus;
        this.isInsideMember = isInsideMember;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    @Id
    @Column(name = "member_id",unique = true,nullable = false)
    @GeneratedValue(generator = "cmsGenerator")
    @GenericGenerator(name = "cmsGenerator", strategy = CmsUpgrader.IDENTIFIER_GENERATOR)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "account")
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "company")
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Column(name = "tel")
    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Column(name = "level_id",insertable = false ,updatable = false)
    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }

    @Column(name = "current_integral")
    public String getCurrentIntegral() {
        return currentIntegral;
    }

    public void setCurrentIntegral(String currentIntegral) {
        this.currentIntegral = currentIntegral;
    }

    @Column(name = "accumulative_integral")
    public String getAccumulativeIntegral() {
        return accumulativeIntegral;
    }

    public void setAccumulativeIntegral(String accumulativeIntegral) {
        this.accumulativeIntegral = accumulativeIntegral;
    }

    @Column(name = "audit_status")
    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    @Column(name = "is_inside_member")
    public String getIsInsideMember() {
        return isInsideMember;
    }

    public void setIsInsideMember(String isInsideMember) {
        this.isInsideMember = isInsideMember;
    }

    @Column(name = "create_time",insertable = false)
    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Column(name = "update_time",updatable = false)
    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
