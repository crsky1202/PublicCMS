package com.publiccms.entities.cms;

import com.publiccms.common.database.CmsUpgrader;
import com.publiccms.common.generator.annotation.GeneratorColumn;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @ Author     ：haoWen
 * @ Date       ：Created in 15:22 2018/9/12
 * @ Description：会员等级
 */
@Entity
@Table(name = "member_level",uniqueConstraints = @UniqueConstraint(columnNames = {"name"}))
@DynamicUpdate
public class MemberLevel implements Serializable {
    private static final long serialVersionUID = 6021596653249155246L;
    //id
    @GeneratorColumn(title = "ID")
    private  long id;

    //名称
    @GeneratorColumn(title = "名称",condition = true,like = true)
    private  String name;

    //升级分数
    @GeneratorColumn(title = "升级分数")
    private  String upgradeScore;

    //积分系数
    @GeneratorColumn(title = "积分系数")
    private  String upgradeCoefficient;

    //能否查看资料
    @GeneratorColumn(title = "能否查询资料")
    private  String isCheckInfo;

    //能否下载资料
    @GeneratorColumn(title = "能否下载资料")
    private  String isDownloadInfo;

    //能否排队检测
    @GeneratorColumn(title = "能否排队检测")
    private  String isLineUpCheck;

    //能否发帖
    @GeneratorColumn(title = "能否发帖")
    private  String isPost;

    //最大下载次数
    @GeneratorColumn(title = "最大下载次数")
    private  String maxDownloadNum;

    //每天最大下载次数
    @GeneratorColumn(title = "每天最大下载次数")
    private  String dailyMaxDownload;

    //提前排队时间
    @GeneratorColumn(title = "提前排队时间")
    private  String queuingInAdvance;

    //创建时间
    @GeneratorColumn(title = "创建时间",order = true)
    private Date createTime;

    //修改时间
    @GeneratorColumn(title = "修改时间")
    private  String updateTime;

    //级别
    @GeneratorColumn(title = "级别")
    private  String level;

    public MemberLevel() {
    }

    public MemberLevel(long id, String name, String upgradeScore, String upgradeCoefficient, String isCheckInfo, String isDownloadInfo, String isLineUpCheck, String isPost, String maxDownloadNum, String dailyMaxDownload, String queuingInAdvance, String level) {
        this.id = id;
        this.name = name;
        this.upgradeScore = upgradeScore;
        this.upgradeCoefficient = upgradeCoefficient;
        this.isCheckInfo = isCheckInfo;
        this.isDownloadInfo = isDownloadInfo;
        this.isLineUpCheck = isLineUpCheck;
        this.isPost = isPost;
        this.maxDownloadNum = maxDownloadNum;
        this.dailyMaxDownload = dailyMaxDownload;
        this.queuingInAdvance = queuingInAdvance;
        this.level = level;
    }

    @Id
    @Column(name = "level_id",unique = true,nullable = false)
    @GeneratedValue(generator = "cmsGenerator")
    @GenericGenerator(name = "cmsGenerator", strategy = CmsUpgrader.IDENTIFIER_GENERATOR)
    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "upgrade_score")
    public String getUpgradeScore() {
        return upgradeScore;
    }


    public void setUpgradeScore(String upgradeScore) {
        this.upgradeScore = upgradeScore;
    }

    @Column(name = "upgrade_coefficient")
    public String getUpgradeCoefficient() {
        return upgradeCoefficient;
    }

    public void setUpgradeCoefficient(String upgradeCoefficient) {
        this.upgradeCoefficient = upgradeCoefficient;
    }

    @Column(name = "is_check_info")
    public String getIsCheckInfo() {
        return isCheckInfo;
    }

    public void setIsCheckInfo(String isCheckInfo) {
        this.isCheckInfo = isCheckInfo;
    }

    @Column(name = "is_download_info")
    public String getIsDownloadInfo() {
        return isDownloadInfo;
    }

    public void setIsDownloadInfo(String isDownloadInfo) {
        this.isDownloadInfo = isDownloadInfo;
    }

    @Column(name = "is_line_up_check")
    public String getIsLineUpCheck() {
        return isLineUpCheck;
    }

    public void setIsLineUpCheck(String isLineUpCheck) {
        this.isLineUpCheck = isLineUpCheck;
    }

    @Column(name = "is_post")
    public String getIsPost() {
        return isPost;
    }

    public void setIsPost(String isPost) {
        this.isPost = isPost;
    }

    @Column(name = "max_download_num")
    public String getMaxDownloadNum() {
        return maxDownloadNum;
    }


    public void setMaxDownloadNum(String maxDownloadNum) {
        this.maxDownloadNum = maxDownloadNum;
    }

    @Column(name = "daily_max_download")
    public String getDailyMaxDownload() {
        return dailyMaxDownload;
    }

    public void setDailyMaxDownload(String dailyMaxDownload) {
        this.dailyMaxDownload = dailyMaxDownload;
    }

    @Column(name = "queuing_in_advance")
    public String getQueuingInAdvance() {
        return queuingInAdvance;
    }

    public void setQueuingInAdvance(String queuingInAdvance) {
        this.queuingInAdvance = queuingInAdvance;
    }

    @Column(name = "create_time")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Column(name = "update_time")
    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "level")
    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
