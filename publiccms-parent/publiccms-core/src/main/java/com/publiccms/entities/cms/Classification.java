package com.publiccms.entities.cms;

import com.publiccms.common.database.CmsUpgrader;
import com.publiccms.common.generator.annotation.GeneratorColumn;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;

/**
 * @ Author     ：haoWen
 * @ Date       ：Created in 17:25 2018/9/12
 * @ Description：分类
 */
@Entity
//UniqueConstraint唯一约束
@Table(name = "classification",uniqueConstraints = @UniqueConstraint(columnNames = {"classification_name"}))
@DynamicUpdate
public class Classification implements Serializable {

    private static final long serialVersionUID = -4603982166139674123L;

    //ID
    @GeneratorColumn(title = "ID",condition = true)
    private  long id;

    //父ID
    @GeneratorColumn(title = "父ID",condition = true)
    private  long parentId;

    //分类名称
    @GeneratorColumn(title = "分类名称",condition = true,like = true)
    private  String classificationName;

    //简介
    @GeneratorColumn(title = "简介",condition = true)
    private  String briefIntroduction;

    //备注
    @GeneratorColumn(title = "备注",condition = true)
    private  String remark;

    //排序
    @GeneratorColumn(title = "排序")
    private  String orderNumber;

    //创建时间
    @GeneratorColumn(title = "创建时间",condition = true,order = true)
    private String createTime;

    //修改时间
    @GeneratorColumn(title = "修改时间")
    private String updateTime;

    public Classification(long id, long parentId, String classificationName, String briefIntroduction, String remark, String orderNumber) {
        this.id = id;
        this.parentId = parentId;
        this.classificationName = classificationName;
        this.briefIntroduction = briefIntroduction;
        this.remark = remark;
        this.orderNumber = orderNumber;
    }

    @Id
    @Column(name = "id",unique = true,nullable = false)
    @GeneratedValue(generator = "cmsGenerator")
    @GenericGenerator(name = "cmsGenerator", strategy = CmsUpgrader.IDENTIFIER_GENERATOR)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "parent_id")
    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    @Column(name = "classification_name")
    public String getClassificationName() {
        return classificationName;
    }

    public void setClassificationName(String classificationName) {
        this.classificationName = classificationName;
    }

    @Column(name = "brief_introduction")
    public String getBriefIntroduction() {
        return briefIntroduction;
    }

    public void setBriefIntroduction(String briefIntroduction) {
        this.briefIntroduction = briefIntroduction;
    }

    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Column(name = "order_number")
    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    @Column(name = "create_time")
    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Column(name = "update_time")
    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
