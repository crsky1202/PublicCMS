package com.publiccms.entities.cms;

import com.publiccms.common.database.CmsUpgrader;
import com.publiccms.common.generator.annotation.GeneratorColumn;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;

/**
 * @ Author     ：haoWen
 * @ Date       ：Created in 16:21 2018/9/12
 * @ Description：加分项目
 */
@Entity
@Table(name = "addition_term")
@DynamicUpdate
public class AdditionTerm implements Serializable {
    private static final long serialVersionUID = 8598084425526810280L;

    //ID
    @GeneratorColumn(title = "ID")
    private  Long id;

    //加分项名称
    @GeneratorColumn(title = "加分项目名称",condition = true,like = true)
    private  String name;

    //分值
    @GeneratorColumn(title = "分值")
    private  String score;

    //创建时间
    @GeneratorColumn(title = "创建时间",order = true)
    private String createTime;

    //修改时间
    @GeneratorColumn(title = "修改时间")
    private String updateTime;

    //排序
    @GeneratorColumn(title = "排序序号")
    private String orderNum;

    public AdditionTerm() {
    }

    public AdditionTerm(Long id, String name, String score, String orderNum) {
        this.id = id;
        this.name = name;
        this.score = score;
        this.orderNum = orderNum;
    }

    @Id
    @Column(name = "id",unique = true,nullable = false)
    @GeneratedValue(generator = "cmsGenerator")
    @GenericGenerator(name = "cmsGenerator", strategy = CmsUpgrader.IDENTIFIER_GENERATOR)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "score")
    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    @Column(name = "create_time")
    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Column(name = "update_time")
    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "order_num")
    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }
}
