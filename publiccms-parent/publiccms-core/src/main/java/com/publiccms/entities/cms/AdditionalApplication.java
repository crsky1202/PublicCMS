package com.publiccms.entities.cms;

import com.publiccms.common.database.CmsUpgrader;
import com.publiccms.common.generator.annotation.GeneratorColumn;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @ Author     ：haoWen
 * @ Date       ：Created in 16:36 2018/9/12
 * @ Description：加分申请
 */
@Entity
@Table(name = "additional_application")
@DynamicUpdate
public class AdditionalApplication implements Serializable {
    private static final long serialVersionUID = 2744841497916085288L;
    //ID
    @GeneratorColumn(title = "id")
    private  Long id;

    //申请人
    @GeneratorColumn(title = "申请人")
    private  String applicant;

    //加分项ID
    @GeneratorColumn(title = "加分项ID")
    private  long additionTermId;

    //分值
    @GeneratorColumn(title = "分值")
    private  String score;

    //创建时间
    @GeneratorColumn(title = "创建时间")
    private String createTime;

    //修改时间
    @GeneratorColumn(title = "修改时间")
    private String updateTime;

    //审核状态
    @GeneratorColumn(title = "审核状态")
    private String auditStatus;

    public AdditionalApplication() {
    }

    public AdditionalApplication(Long id, String applicant, long additionTermId, String score, String auditStatus) {
        this.id = id;
        this.applicant = applicant;
        this.additionTermId = additionTermId;
        this.score = score;
        this.auditStatus = auditStatus;
    }

    @Id
    @Column(name = "id",unique = true,nullable = false)
    @GeneratedValue(generator = "cmsGenerator")
    @GenericGenerator(name = "cmsGenerator", strategy = CmsUpgrader.IDENTIFIER_GENERATOR)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "applicant")
    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    @Column(name = "addition_term_id")
    public long getAdditionTermId() {
        return additionTermId;
    }

    public void setAdditionTermId(long additionTermId) {
        this.additionTermId = additionTermId;
    }

    @Column(name = "score")
    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    @Column(name = "create_time")
    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Column(name = "update_time")
    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "audit_status")
    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }
}
