package com.publiccms.entities.cms;

import com.publiccms.common.database.CmsUpgrader;
import com.publiccms.common.generator.annotation.GeneratorColumn;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @ Author     ：haoWen
 * @ Date       ：Created in 13:30 2018/9/11
 * @ Description：会员
 */
@Entity
@Table(name = "commentary", uniqueConstraints = @UniqueConstraint(columnNames = {"id"}))
@DynamicUpdate
public class Commentary implements Serializable {

    private static final long serialVersionUID = 8917849561822541696L;
    @GeneratorColumn(title = "ID")
    private Long id;

    @GeneratorColumn(title = "账号")
    private String account;

    @GeneratorColumn(title = "标题", condition = true, like = true)
    private String title;

    @GeneratorColumn(title = "主题")
    private String theme;

    @GeneratorColumn(title = "内容")
    private String content;

    @GeneratorColumn(title = "邮箱")
    private String email;

    @GeneratorColumn(title = "创建时间", order = true)
    private String createTime;

    @GeneratorColumn(title = "修改时间")
    private String updateTime;

    public Commentary() {
    }

    public Commentary(Long id, String account, String title, String theme, String content, String createTime, String updateTime) {
        this.id = id;
        this.account = account;
        this.title = title;
        this.theme = theme;
        this.content = content;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(generator = "cmsGenerator")
    @GenericGenerator(name = "cmsGenerator", strategy = CmsUpgrader.IDENTIFIER_GENERATOR)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "account")
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "theme")
    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "create_time", insertable = false)
    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Column(name = "update_time", updatable = false)
    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
