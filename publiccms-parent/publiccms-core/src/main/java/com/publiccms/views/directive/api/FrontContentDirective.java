package com.publiccms.views.directive.api;

import com.publiccms.common.base.AbstractAppDirective;
import com.publiccms.common.handler.RenderHandler;
import com.publiccms.entities.cms.CmsContent;
import com.publiccms.entities.cms.CmsContentAttribute;
import com.publiccms.entities.sys.SysApp;
import com.publiccms.entities.sys.SysUser;
import com.publiccms.logic.service.cms.CmsContentAttributeService;
import com.publiccms.logic.service.cms.CmsContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * ContentFrontDirective
 */
@Component
public class FrontContentDirective extends AbstractAppDirective {
    @Autowired
    private CmsContentService contentService;
    @Autowired
    private CmsContentAttributeService contentAttributeService;

    @Override
    public void execute(RenderHandler handler, SysApp app, SysUser user) throws IOException, Exception {
        Long id = handler.getLong("id");
        CmsContent content = contentService.getEntity(id);
        int preClicks = content.getClicks();
        content.setClicks(preClicks++);
        CmsContentAttribute contentAttribute = contentAttributeService.getEntity(id);
        content.setModelId(content.getEditor());         //编辑人
        content.setEditor(contentAttribute.getText());  //内容
        handler.put("result", content);
    }

    @Override
    public boolean needUserToken() {
        return false;
    }

    @Override
    public boolean needAppToken() {
        return false;
    }
}