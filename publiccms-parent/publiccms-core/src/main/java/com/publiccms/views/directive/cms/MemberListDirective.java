package com.publiccms.views.directive.cms;

// Generated 2018-9-14 10:36:33 by com.publiccms.common.generator.SourceGenerator
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.publiccms.logic.service.cms.MemberService;
import com.publiccms.common.base.AbstractTemplateDirective;
import com.publiccms.common.handler.RenderHandler;
import com.publiccms.common.handler.PageHandler;

/**
 *
 * MemberListDirective
 * 
 */
@Component
public class MemberListDirective extends AbstractTemplateDirective {
    private final MemberService service;

    @Autowired
    public MemberListDirective(MemberService service) {
        this.service = service;
    }

    @Override
    public void execute(RenderHandler handler) throws Exception {
        PageHandler page = service.getPage(handler.getString("name"), handler.getString("auditStatus"), handler.getString("isInsideMember"),
                handler.getString("orderType"), handler.getInteger("pageIndex",1), handler.getInteger("count",30));
        handler.put("page", page).render();
    }
}