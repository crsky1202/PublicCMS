package com.publiccms.views.directive.cms;

// Generated 2018-9-14 10:36:33 by com.publiccms.common.generator.SourceGenerator

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.publiccms.entities.cms.Member;
import com.publiccms.logic.service.cms.MemberService;
import com.publiccms.common.tools.CommonUtils;
import com.publiccms.common.base.AbstractTemplateDirective;
import com.publiccms.common.handler.RenderHandler;

/**
 *
 * MemberDirective
 * 
 */
@Component
public class MemberDirective extends AbstractTemplateDirective {

    private final MemberService service;
    @Autowired
    public MemberDirective(MemberService service) {
        this.service = service;
    }

    @Override
    public void execute(RenderHandler handler) throws Exception {
        Long id = handler.getLong("id");
        if (CommonUtils.notEmpty(id)) {
            handler.put("object", service.getEntity(id)).render();
        } else {
            Integer[] ids = handler.getIntegerArray("ids");
            if (CommonUtils.notEmpty(ids)) {
                List<Member> entityList = service.getEntitys(ids);
                Map<String, Member> map = new LinkedHashMap<>();
                for (Member entity : entityList) {
                    map.put(String.valueOf(entity.getId()), entity);
                }
                handler.put("map", map).render();
            }
        }
    }
}
