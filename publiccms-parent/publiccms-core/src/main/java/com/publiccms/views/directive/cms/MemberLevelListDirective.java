package com.publiccms.views.directive.cms;

// Generated 2018-9-13 23:37:54 by com.publiccms.common.generator.SourceGenerator
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.publiccms.logic.service.cms.MemberLevelService;
import com.publiccms.common.base.AbstractTemplateDirective;
import com.publiccms.common.handler.RenderHandler;
import com.publiccms.common.handler.PageHandler;

/**
 *
 * MemberLevelListDirective
 * 
 */
@Component
public class MemberLevelListDirective extends AbstractTemplateDirective {
    private final MemberLevelService service;

    @Autowired
    public MemberLevelListDirective(MemberLevelService service) {
        this.service = service;
    }

    @Override
    public void execute(RenderHandler handler) throws Exception {
        PageHandler page = service.getPage(handler.getString("name"), 
                handler.getString("orderType"), handler.getInteger("pageIndex",1), handler.getInteger("count",30));
        handler.put("page", page).render();
    }
}