package com.publiccms.views.directive.api;

import com.publiccms.common.base.AbstractAppDirective;
import com.publiccms.common.handler.RenderHandler;
import com.publiccms.common.tools.DateUtils;
import com.publiccms.entities.cms.Commentary;
import com.publiccms.entities.sys.SysApp;
import com.publiccms.entities.sys.SysUser;
import com.publiccms.logic.service.cms.CommentaryService;
import com.publiccms.logic.service.log.LogOperateService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

/**
 * ContentFrontDirective
 */
@Component
public class FrontCommentaryDirective extends AbstractAppDirective {

    @Autowired
    protected LogOperateService logOperateService;
    @Autowired
    private CommentaryService service;

    @Override
    public boolean needUserToken() {
        return false;
    }

    @Override
    public void execute(RenderHandler handler, SysApp app, SysUser user) throws IOException, Exception {
        String account = StringUtils.trim(handler.getString("account"));
        String title = StringUtils.trim(handler.getString("title"));
        String theme = StringUtils.trim(handler.getString("theme"));
        String content = StringUtils.trim(handler.getString("content"));
        Commentary entity = new Commentary();
        entity.setAccount(account);
        entity.setTitle(title);
        entity.setTheme(theme);
        entity.setContent(content);
        entity.setCreateTime(DateUtils.dateToString(new Date()));
        service.save(entity);
        handler.put("result", 0);
    }

    @Override
    public boolean needAppToken() {
        return false;
    }
}