package com.publiccms.views.directive.cms;

// Generated 2018-9-16 11:41:23 by com.publiccms.common.generator.SourceGenerator
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.publiccms.logic.service.cms.AdditionalApplicationService;
import com.publiccms.common.base.AbstractTemplateDirective;
import com.publiccms.common.handler.RenderHandler;
import com.publiccms.common.handler.PageHandler;

/**
 *
 * AdditionalApplicationListDirective
 * 
 */
@Component
public class AdditionalApplicationListDirective extends AbstractTemplateDirective {

    private final AdditionalApplicationService service;

    @Autowired
    public AdditionalApplicationListDirective(AdditionalApplicationService service) {
        this.service = service;
    }

    @Override
    public void execute(RenderHandler handler) throws IOException, Exception {
        PageHandler page = service.getPage(handler.getString("name"),
                handler.getInteger("pageIndex",1), handler.getInteger("count",30));
        handler.put("page", page).render();
    }
}