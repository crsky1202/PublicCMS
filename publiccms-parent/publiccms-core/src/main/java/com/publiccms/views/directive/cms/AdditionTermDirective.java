package com.publiccms.views.directive.cms;

// Generated 2018-9-16 11:35:04 by com.publiccms.common.generator.SourceGenerator

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.publiccms.entities.cms.AdditionTerm;
import com.publiccms.logic.service.cms.AdditionTermService;
import com.publiccms.common.tools.CommonUtils;
import com.publiccms.common.base.AbstractTemplateDirective;
import com.publiccms.common.handler.RenderHandler;

/**
 *
 * AdditionTermDirective
 * 
 */
@Component
public class AdditionTermDirective extends AbstractTemplateDirective {

    private final AdditionTermService service;

    @Autowired
    public AdditionTermDirective(AdditionTermService service) {
        this.service = service;
    }

    @Override
    public void execute(RenderHandler handler) throws IOException, Exception {
        Integer id = handler.getInteger("id");
        if (CommonUtils.notEmpty(id)) {
            handler.put("object", service.getEntity(id)).render();
        } else {
            Integer[] ids = handler.getIntegerArray("ids");
            if (CommonUtils.notEmpty(ids)) {
                List<AdditionTerm> entityList = service.getEntitys(ids);
                Map<String, AdditionTerm> map = new LinkedHashMap<>();
                for (AdditionTerm entity : entityList) {
                    map.put(String.valueOf(entity.getId()), entity);
                }
                handler.put("map", map).render();
            }
        }
    }
}
