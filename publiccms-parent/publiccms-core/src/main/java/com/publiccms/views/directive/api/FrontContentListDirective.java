package com.publiccms.views.directive.api;

import com.publiccms.common.base.AbstractAppDirective;
import com.publiccms.common.handler.PageHandler;
import com.publiccms.common.handler.RenderHandler;
import com.publiccms.entities.sys.SysApp;
import com.publiccms.entities.sys.SysUser;
import com.publiccms.logic.service.cms.CmsContentService;
import com.publiccms.views.pojo.query.CmsContentQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * FrontContentListDirective
 */
@Component
public class FrontContentListDirective extends AbstractAppDirective {

    @Override
    public void execute(RenderHandler handler, SysApp app, SysUser user) throws IOException, Exception {
        PageHandler page = null;
        try {
            page = service.getPage(
                    new CmsContentQuery(getSite(handler).getId(), handler.getIntegerArray("status"), handler.getInteger("categoryId"),
                            handler.getIntegerArray("categoryIds"), false, null, handler.getLong("parentId"),
                            handler.getBoolean("emptyParent"), handler.getBoolean("onlyUrl"), handler.getBoolean("hasImages"),
                            handler.getBoolean("hasCover"), handler.getBoolean("hasFiles"), null, null, null,
                            handler.getDate("endPublishDate")),
                    handler.getBoolean("containChild"), "publishDate", "desc", handler.getInteger("pageIndex"),
                    handler.getInteger("count"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        handler.put("page", page);
    }

    @Autowired
    private CmsContentService service;

    @Override
    public boolean needUserToken() {
        return false;
    }

    @Override
    public boolean needAppToken() {
        return false;
    }
}