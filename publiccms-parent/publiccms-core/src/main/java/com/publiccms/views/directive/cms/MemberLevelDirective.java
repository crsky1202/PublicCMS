package com.publiccms.views.directive.cms;

// Generated 2018-9-13 23:37:54 by com.publiccms.common.generator.SourceGenerator

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.publiccms.entities.cms.MemberLevel;
import com.publiccms.logic.service.cms.MemberLevelService;
import com.publiccms.common.tools.CommonUtils;
import com.publiccms.common.base.AbstractTemplateDirective;
import com.publiccms.common.handler.RenderHandler;

/**
 *
 * MemberLevelDirective
 * 
 */
@Component
public class MemberLevelDirective extends AbstractTemplateDirective {

    private final MemberLevelService service;

    @Autowired
    public MemberLevelDirective(MemberLevelService service) {
        this.service = service;
    }

    @Override
    public void execute(RenderHandler handler) throws Exception {
        Long id = handler.getLong("id");
        if (CommonUtils.notEmpty(id)) {
            handler.put("object", service.getEntity(id)).render();
        } else {
            Integer[] ids = handler.getIntegerArray("ids");
            if (CommonUtils.notEmpty(ids)) {
                List<MemberLevel> entityList = service.getEntitys(ids);
                Map<String, MemberLevel> map = new LinkedHashMap<>();
                for (MemberLevel entity : entityList) {
                    map.put(String.valueOf(entity.getId()), entity);
                }
                handler.put("map", map).render();
            }
        }
    }
}
