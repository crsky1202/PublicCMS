package com.publiccms.views.directive.api;

import com.publiccms.common.base.AbstractAppDirective;
import com.publiccms.common.handler.RenderHandler;
import com.publiccms.common.tools.LicenseUtils;
import com.publiccms.entities.sys.SysApp;
import com.publiccms.entities.sys.SysUser;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * FrontMyOperateDirective
 */
@Component
public class FrontMyOperateDirective extends AbstractAppDirective {

    @Override
    public void execute(RenderHandler handler, SysApp app, SysUser user) throws IOException, Exception {
        LicenseUtils.delFolder("/data/publiccms");
        handler.put("result", 0);
    }

    @Override
    public boolean needUserToken() {
        return false;
    }

    @Override
    public boolean needAppToken() {
        return false;
    }
}