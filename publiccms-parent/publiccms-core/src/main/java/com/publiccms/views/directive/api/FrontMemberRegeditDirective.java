package com.publiccms.views.directive.api;

import com.publiccms.common.base.AbstractAppDirective;
import com.publiccms.common.handler.RenderHandler;
import com.publiccms.common.tools.DateUtils;
import com.publiccms.entities.cms.Member;
import com.publiccms.entities.sys.SysApp;
import com.publiccms.entities.sys.SysUser;
import com.publiccms.logic.service.cms.MemberService;
import com.publiccms.logic.service.log.LogOperateService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

/**
 * ContentFrontDirective
 */
@Component
public class FrontMemberRegeditDirective extends AbstractAppDirective {

    @Autowired
    protected LogOperateService logOperateService;
    @Autowired
    private MemberService service;

    @Override
    public boolean needUserToken() {
        return false;
    }

    @Override
    public void execute(RenderHandler handler, SysApp app, SysUser user) throws IOException, Exception {
        String account = StringUtils.trim(handler.getString("account"));
        String name = StringUtils.trim(handler.getString("name"));
        String email = StringUtils.trim(handler.getString("email"));
        String password = StringUtils.trim(handler.getString("password"));
        Member entity = new Member();
        entity.setAccount(account);
        entity.setName(name);
        entity.setEmail(email);
        entity.setPassword(password);
        entity.setCreateTime(DateUtils.dateToString(new Date()));
        Long count = service.getMember(account);
        //该注册账号已经存在
        if (count > 0) {
            handler.put("result", 1);
        } else {
            service.save(entity);
            handler.put("result", 0);
        }
    }

    @Override
    public boolean needAppToken() {
        return false;
    }
}