package com.publiccms.views.directive.cms;

// Generated 2018-9-16 11:41:23 by com.publiccms.common.generator.SourceGenerator

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.publiccms.entities.cms.AdditionalApplication;
import com.publiccms.logic.service.cms.AdditionalApplicationService;
import com.publiccms.common.tools.CommonUtils;
import com.publiccms.common.base.AbstractTemplateDirective;
import com.publiccms.common.handler.RenderHandler;

/**
 *
 * AdditionalApplicationDirective
 * 
 */
@Component
public class AdditionalApplicationDirective extends AbstractTemplateDirective {

    private final AdditionalApplicationService service;

    @Autowired
    public AdditionalApplicationDirective(AdditionalApplicationService service) {
        this.service = service;
    }

    @Override
    public void execute(RenderHandler handler) throws IOException, Exception {
        Integer id = handler.getInteger("id");
        if (CommonUtils.notEmpty(id)) {
            handler.put("object", service.getEntity(id)).render();
        } else {
            Integer[] ids = handler.getIntegerArray("ids");
            if (CommonUtils.notEmpty(ids)) {
                List<AdditionalApplication> entityList = service.getEntitys(ids);
                Map<String, AdditionalApplication> map = new LinkedHashMap<>();
                for (AdditionalApplication entity : entityList) {
                    map.put(String.valueOf(entity.getId()), entity);
                }
                handler.put("map", map).render();
            }
        }
    }
}
